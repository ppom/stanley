import Settings from './Settings.svelte';

const settings = new Settings({
	target: document.body,
	props: {}
});

export default settings;
