import App from './App.svelte';

const app = new App({
  target: document.body,
  props: { textmode: true }
});

export default app;
