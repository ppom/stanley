export const COLORS = [
  "#522ad6",
  "#2a4fd6",
  "#508c2f",
  "#d87a15",
  "#e5d309",
  "#8c1b26",
];

export const START_COLOR = "#c8a078";

export const END_COLOR = "#000000";

export const WORDS = [
  "caresser",
  "creuser",
  "déposer",
  "escalader",
  "fermer",
  "frôler",
  "glissant",
  "oppressant",
  "ouvrir",
  "pousser",
  "prendre",
  "rebondissant",
  "sculpter",
  "spiralant",
  "supporter",
  "survoler",
  "tanguant",
  "trancher",
  "écouter",
  "écraser",
];

export const LAST_WORD = "quitter";
export const END_WORD = "caméléon";

export const SONGS = [
  "cours.danse",
  "edipo.re",
  "la.fanmi",
  "machine.gun",
  "pongo.bruxos",
  "pont.des.arts",
  "reality.cuts.me.like.a.knife",
  "resiste",
  "silence",
  "son.qual.nave",
  "stanley.pour.paco.1",
  "stanley.pour.paco.2",
  "tarantelle",
  "the.dance.inside",
  "un.lapin",
];

export let AUDIOS = SONGS.map(s => `audio/${s}.mp3`);
shuffleArray(AUDIOS);

export const LENGTHES = [
  // secondes
  20,
  30,
  45,
];

// secondes
export const TOTAL_LENGTH = 60 * 10;

export const SQUARE_LENGTH = 5;

if (TOTAL_LENGTH > 4 * LENGTHES[0] * SONGS.length) {
  console.warn("Wait, you may lack some audios in the end");
  console.warn("Total: " + TOTAL_LENGTH);
  console.warn("Min length: " + LENGTHES[0] * SONGS.length);
}

const randomThing = array => array[
  Math.floor((Math.random() * array.length))
];

const pad = number => (number >= 10 ? "" : "0") + number;

// found here: https://jsfiddle.net/salman/f9Re3/
export const invert = color => {
  color = color.substring(1); // remove #
  color = parseInt(color, 16); // convert to integer
  color = 0xFFFFFF ^ color; // invert three bytes
  color = color.toString(16); // convert to hex
  color = ("000000" + color).slice(-6); // pad with leading zeros
  color = "#" + color; // prepend #
  return color;
}

export const randomWord = () => randomThing(WORDS);

export const randomPeriod = () => randomThing(LENGTHES);

export const randomAudio = () => AUDIOS.pop();

export const randomHour = () => pad(Math.floor(Math.random() * 13)) + ":" + pad(Math.floor(Math.random() * 60));

export const randomColor = oldColor => {
  const color = randomThing(COLORS.filter(c => c != oldColor));
  const textColor = invert(color);
  return { color, textColor };
};

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}
