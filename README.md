# Dés

Ce site affiche des mots, couleurs et sons aléatoires.

Le site est disponible à [cette adresse](https://u.ppom.me/stanley/).

La liste des paramètres est disponible à [cette adresse](https://u.ppom.me/stanley/settings.html).

## Lancer le projet

Avoir node et npm installés. Puis :
```bash
# Installer les dépendances
npm install
# Serveur de développement
npm run dev
# build optimisé
npm run build
```
